package com.example.anil.a2way_databinding_demo_101;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;
import android.view.View;

public class UserViewModel extends BaseObservable {

    private String name ;

    @Bindable
    public String getName() {
        Log.d("viewmodel", "getname called "+name);
        return name;
    }

    public void setName(String value) {

        Log.d("viewmodel", "setname called" + name);
        // Avoids infinite loops.
        if (name != value) {
            name = value;

            // React to the change.

            // Notify observers of a new value.
            notifyPropertyChanged(BR.name);
        }

    }

    public void onClick(View view)
    {
        setName("hey ak");
    }



}